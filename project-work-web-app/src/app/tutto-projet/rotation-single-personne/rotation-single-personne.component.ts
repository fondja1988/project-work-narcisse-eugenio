import { Component, OnInit, Input } from '@angular/core';
import { Personne } from 'src/app/model';

@Component({
  selector: 'app-rotation-single-personne',
  templateUrl: './rotation-single-personne.component.html',
  styleUrls: ['./rotation-single-personne.component.css']
})
export class RotationSinglePersonneComponent implements OnInit {
@Input() singlePerson:Personne;
  constructor() { }

  ngOnInit(): void {
  }

}
