import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Personne } from 'src/app/model';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/api.service';

@Component({
  selector: 'app-item-personne',
  templateUrl: './item-personne.component.html',
  styleUrls: ['./item-personne.component.css']
})
export class ItemPersonneComponent implements OnInit {
  personneItem:Personne[];
  message = '';
  @Output() evenement = new EventEmitter();
  constructor(
               private route:Router,
               //injection du service web
               private mongetdato:ApiService) { }

  ngOnInit(): void {
    //this.personneItem = this.personService.getpersonneService();
    this.mongetdato.getAllDati().subscribe(
      (observer)=>{ 
        this.personneItem = observer;
      },
      (erreur)=>{ this.message = " ERROR OF CONNEXION AT SERVER, PLEASE VERIFY YOUR CONNEXION AND TRY AGAIN. "}
    )
  }
  affiche(element:Personne){
    this.evenement.emit(element);
    
  }
  cambia(element:Personne){
    const link = ['detail/' + element.id]
    this.route.navigate(link);
  }

}
