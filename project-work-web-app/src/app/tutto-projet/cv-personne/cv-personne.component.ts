import { Component, OnInit, Output } from '@angular/core';
import { Personne } from 'src/app/model';
import { interval } from 'rxjs';

@Component({
  selector: 'app-cv-personne',
  templateUrl: './cv-personne.component.html',
  styleUrls: ['./cv-personne.component.css']
})
export class CvPersonneComponent implements OnInit {
 personeCv:Personne;
 timer$sub = null;
 message = "WELCOME ON CUSTOMER APP";
  constructor() { }

  ngOnInit(): void {
    const timer$ = interval(1000);
     const interotto = timer$.subscribe( (valore:any)=>{
     // console.log(valore);
      if(valore %2 ===0){
        this.timer$sub = this.message;
      }
      else{
        this.timer$sub = '';
      }
//fermare la stampa tempornea dei dati
       setTimeout( ()=>{ interotto.unsubscribe()
        this.timer$sub = this.message;
      },20000);
    });
   
  }
  eventCv(event:Personne){
    this.personeCv = event;
  }
  // avec angular, lorsque tu veux insérer une image en background, il faut penser à 
  //   montrer le chemin du dossier en partant de la racine 'src'.
  //   Par contre dans le template, il faut passer par le dossier 'asset'

}
