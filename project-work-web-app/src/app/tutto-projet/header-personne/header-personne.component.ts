import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header-personne',
  templateUrl: './header-personne.component.html',
  styleUrls: ['./header-personne.component.css']
})
export class HeaderPersonneComponent implements OnInit {

  visibility = false;
  constructor() { }

  ngOnInit(): void {
  }
  boutonDeplier(){
    this.visibility = !this.visibility;
  }

}
