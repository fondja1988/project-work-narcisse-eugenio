import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Personne } from 'src/app/model';

@Component({
  selector: 'app-list-personne',
  templateUrl: './list-personne.component.html',
  styleUrls: ['./list-personne.component.css']
})
export class ListPersonneComponent implements OnInit {
@Output() evenementList = new EventEmitter();
  constructor() { }

  ngOnInit(): void {
  }

  evenList(event:Personne){
    
    this.evenementList.emit(event);
  }
}
