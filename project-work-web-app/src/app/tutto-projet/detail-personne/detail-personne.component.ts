import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Personne } from 'src/app/model';
import { ApiService } from 'src/app/api.service';

@Component({
  selector: 'app-detail-personne',
  templateUrl: './detail-personne.component.html',
  styleUrls: ['./detail-personne.component.css']
})
export class DetailPersonneComponent implements OnInit {
  person:Personne;
  personnes:Personne[];
  messageError1 = '';
  messageErrorDelete = '';
  constructor(private route:Router,
              private flux:ActivatedRoute,
              private mongetdato:ApiService) { }

  ngOnInit(): void {
   // this.personnes = this.service.getpersonneService();
   this.mongetdato.getAllDati().subscribe(
     (observer)=>{
       this.personnes = observer;

       this.flux.params.subscribe( (dato)=>{
        for (let i = 0; i<this.personnes.length;i++){
          const valeur = this.personnes[i];
          if(valeur.id == dato.id){
            this.person = valeur;
            return this.person;
          }
        }
     
      })
     },
     (erreur)=>{ this.messageError1 = " ERROR OF CONNEXION TO SERVER, PLEASE VERIFY YOUR INTERNET CONNEXION AND TRY AGAIN. "}
   )
     
  }
  goBack(){
    const link = [''];
    this.route.navigate(link);
  }
  
  delete(person:Personne){
    this.mongetdato.deleteDato(person.id).subscribe( 
      ()=>{
        const link = [''];
        this.route.navigate(link);
      },
      (erreur)=>{ this.messageErrorDelete = " SORRY, THE DELETING FAILS, PLEASE TRY AGAIN LATER. "}
    )
  }

}
