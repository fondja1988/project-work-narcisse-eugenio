import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Personne } from './model';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  link = 'http://localhost:8000/api/users';
  monObservable:Observable<Personne>;

  constructor(private http:HttpClient) { }

  //recuperation de la getall
  getAllDati():Observable<Personne[]>{
    return this.http.get<Personne[]>(this.link);
  }
  //insersion des donnees
   
  postDati(dato:Personne){
    return this.http.post(this.link, dato);
  }

  //suppression d'une donnee
  deleteDato(id:number){
    return this.http.delete(this.link + `/${id}`)
  }
}
