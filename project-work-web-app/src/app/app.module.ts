import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Narcisse } from './routing';
import { FormsModule } from '@angular/forms';
import {  HttpClientModule } from '@angular/common/http';
/**installation de fontawesome */
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core'; 
import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome'; 
import { fas } from '@fortawesome/free-solid-svg-icons'; 
import { far } from '@fortawesome/free-regular-svg-icons'; 

/**installation de bootstrap */
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { CvPersonneComponent } from './tutto-projet/cv-personne/cv-personne.component';
import { ListPersonneComponent } from './tutto-projet/list-personne/list-personne.component';
import { RotationSinglePersonneComponent } from './tutto-projet/rotation-single-personne/rotation-single-personne.component';
import { ItemPersonneComponent } from './tutto-projet/item-personne/item-personne.component';
import { HeaderPersonneComponent } from './tutto-projet/header-personne/header-personne.component';
import { ErrorComponent } from './error/error.component';
import { AddProfileComponent } from './add-profile/add-profile.component';
import { DetailPersonneComponent } from './tutto-projet/detail-personne/detail-personne.component';
import { FooterComponent } from './footer/footer.component';



@NgModule({
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ], 
  declarations: [
    AppComponent,
    CvPersonneComponent,
    ListPersonneComponent,
    RotationSinglePersonneComponent,
    ItemPersonneComponent,
    HeaderPersonneComponent,
    ErrorComponent,
    AddProfileComponent,
    DetailPersonneComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    FontAwesomeModule,
    NgbModule,
   Narcisse,
   FormsModule,
   HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { 
  constructor(library: FaIconLibrary) {    library.addIconPacks(fas, far);  } 
}
