export class Personne{
    constructor(public id:number, public lastname:string, 
                public firstname:string, public age:number = 0, 
                public city:string, public phone_number:number, 
                public email:string){}
}