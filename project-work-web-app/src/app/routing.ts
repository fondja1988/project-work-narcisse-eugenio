import { Routes, RouterModule } from "@angular/router";
import { CvPersonneComponent } from './tutto-projet/cv-personne/cv-personne.component';
import { ErrorComponent } from './error/error.component';
import { AddProfileComponent } from './add-profile/add-profile.component';
import { DetailPersonneComponent } from './tutto-projet/detail-personne/detail-personne.component';

const routes:Routes = [
    {path:'', component:CvPersonneComponent},
    {path:'add', component:AddProfileComponent},
    {path:'detail/:id', component:DetailPersonneComponent},
    {path:'**', component:ErrorComponent}
];

//exportons la route vers le module en nous servant du ModuleRouter

export const Narcisse = RouterModule.forRoot(routes);