import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { Observable } from 'rxjs';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Personne } from '../model';

@Component({
  selector: 'app-add-profile',
  templateUrl: './add-profile.component.html',
  styleUrls: ['./add-profile.component.css']
})
export class AddProfileComponent implements OnInit {
  monObservable:Observable<Personne>;
  message = '';
  constructor(private monpostdato:ApiService,
              private route:Router) { }

  ngOnInit(): void {
    
  }

 create(form:Personne){
   this.monpostdato.postDati(form).subscribe(
     ()=>{
       console.log(form);
       const link = [''];
       this.route.navigate(link);
     },
     (erreur)=>{
       this.message = " ERROR OF CONNEXION TO SERVER, VERIFY YOUR INTERNET CONNEXION AND TRY AGAIN. ";
       console.log(erreur);
     }
   )
 }
}
           