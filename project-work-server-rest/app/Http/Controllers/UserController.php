<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\User;

class UserController extends Controller
{
    public function store(Request $request){
        
        $validator= Validator::make($request->all(), [
            'lastname'=>'required|max:25',
            'firstname'=>'required|max:25',
            'age'=>'required|integer|max:150',
            'city'=>'required',
            'email' => 'required|email:rfc,dns',
            'phone_number'=>'required'
        ]);

        if ($validator->fails()){
            return response()->json([
                'errors'=> $validator->errors()
            ], 400);
        }

        $user=new User();
        $user->lastname = $request->input('lastname');
        $user->firstname = $request->input('firstname');
        $user->age = $request->input('age');
        $user->city = $request->input('city');
        $user->email = $request->input('email');
        $user->phone_number = $request->input('phone_number');
        try{
            $user->save();
        }catch(\Exception $e){
            return response()->json(['error'=>$e -> getMessage()], 400);
        }
        return $user;
    }
    public function getAll(Request $request){
        $users = User::get();
        return $users;
    }
    public function get(Request $request, $id){
        $user= User::findOrFail($id);
        return $user;
    }
    public function delete(Request $request, $id){
        $user= User::findOrFail($id);
        $user-> delete();
        return response()->json(null, 204);
    }
}
