<?php

namespace App\Http\Middleware;

use Closure;

class CORS
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $reponse = $next($request);

        $reponse ->headers ->set('Access-Control-Allow-Origin','*');
        $reponse ->headers ->set('Access-Control-Allow-Methods','*');
        $reponse ->headers ->set('Access-Control-Allow-Headers','Content-Type, Accept,
        Authorization, X-Requested-With, Application');

        return $next($request);
    }
}
