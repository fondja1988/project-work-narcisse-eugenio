<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


Route::post('/users', 'UserController@store');
Route::get('/users', 'UserController@getAll');
Route::get('/users/{id}', 'UserController@get');
Route::delete('/users/{id}', 'UserController@delete');
